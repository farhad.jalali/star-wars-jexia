# Star Wars Jexia

Written by Angular 2, Farhad Jalali, 11th October 2018

## Build

Run `ng build` to build the project (maybe you need to `ng install` first). The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## How to run

Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`.

## Services

people.service.ts:   Fetching people from 'swapi' and all data organizing will be handled by this service

## Components

app:      master component
top-bar:  logo + navigation menus
home:     starting page
people:   show, sort and filter the whole people
person:   show the person details
planet:    just planet part of a person details info

## Running unit tests

Run `ng test` to execute the unit tests via [Karma].

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor].
