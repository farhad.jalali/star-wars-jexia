import { Injectable } from '@angular/core';
import { Person } from './person';
import { Planet } from './planet';
import { Observable, of, pipe } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root',
})
export class PeopleService {

  // After fetching people from 'swapi' page by page, we keep all loaded people in memory.
  people: Person[]

  // Used for toggling between asc and desc sort
  sortField: string

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
  }

  /**
   * handles http errors while fetching data from 'swapi'
   * @param operation
   * @param result
   */
  private handleHttpError<T>(message: string, result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.messageService.message = `${message}: ${error.message}`;
      return of(result as T);
    };
  }

  /**
   * After fetching persons from 'swapi' we need to organize some properties such as id, birth year and planet
   * @param persons
   */
  private organizePersons(persons: Person[]) {
    persons.forEach(person => {
      // https:/swapi.co/api/people/9/   => {people/9} {9}
      var match = person.url.match(/people\/(\d+)/);
      person.id = +match[1];

      // 112.3BBY   => {112.3} {112.3}
      var match = person.birth_year.match(/[\d\.]+/);
      person.numericBirthYear = match ? parseFloat(match[0]) : 0;

      this.fetchPlanetOfPerson(person);
    });

    // push loaded persons to people list
    this.people = this.people.concat(persons);
  }

  /**
   * After fetching a person, we need to fetch its planet based on the planet url property
   * @param person
   */
  private fetchPlanetOfPerson(person: Person) {
    // for having more friendly UI, until fetching completion we show checking ... as its planet name
    person.planet = { name: "checking..." } as Planet;

    this.http
      .get<any>(person.homeworld)
      .pipe(
        tap(planet => { }),
        catchError(this.handleHttpError(`Swapi error [${person.homeworld}]. please check the web site.`, { name: "<ERROR>" }))
      )
      .subscribe(res => person.planet = res);
  }

  /**
   * Fetch people page by page automatically until the end of list (when result's next property is null)
   * @param url
   * @param next
   * @param complete
   */
  private fetchPeople(url: string, next: () => void, complete: (err?) => void) {
    this.http
      .get<any>(url)
      .pipe(
        tap(res => {
          // organazing the fetched people part and pushing them into people property
          this.organizePersons(res.results);

          // calls the next callback to update UI by the loaded part
          next();

          // while there is another page to load, we call fetchPeople continuesly
          if (res.next)
            this.fetchPeople(res.next, next, complete);
          else { // end of the people's list
            complete();
          }
        }),
        catchError(this.handleHttpError(`Swapi error [${url}] please check the web site.`, { "results": [{ name: "<ERROR>" }] }))
      )
      .subscribe();
  }

  /**
   * Sorts people list based on given field
   * @param field
   */
  public sortPeople(field: string): Person[] {
    this.people.sort((a: Person, b: Person) => {
      var value;
      switch (field) {
        case "numericBirthYear":
          value = a[field] === b[field] ? 0 : a[field] > b[field] ? 1 : -1;
          break;

        case "planet":
          if (a.planet && b.planet)
            value = ('' + a.planet.name).localeCompare(b.planet.name);
          else
            value = 0;
          break;

        default:
          value = ('' + a[field]).localeCompare(b[field]);
          break;
      }

      // Toggle sort Asc <> Des if click on a field twice
      return this.sortField === field ? -value : value;
    });

    this.sortField = this.sortField === field ? "" : field;
    return this.people;
  }

  /**
   * Filters the fetched people based on the given phrase
   * @param filter
   * @param people
   */
  private filterPeople(filter: string, people: Person[]): Person[] {
    filter = filter.toLowerCase();
    if (/^[\d\.]+$/.test(filter)) // If we enter a number, it would be birth year filter
      return people.filter(person => person.birth_year === filter + "BBY");
    else
      return people.filter(person =>
        person.gender === filter ||
        person.name.toLowerCase().indexOf(filter) > -1 ||
        (person.planet && person.planet.name.toLowerCase().indexOf(filter) > -1)
      );
  }

  /**
   *  fetch people and update component page by page */
  public getPeople(filter: string): Observable<Person[]> {
    if (this.people) { // if people property has been initialized, it means we've already started the http fetching function
      return new Observable<Person[]>((observer) => {
        // First notify the component about the loaded people
        var filteredPeople = this.filterPeople(filter, this.people);
        observer.next(filteredPeople);
        observer.complete();
        this.messageService.message = "People was fetched.";
      });
    }
    else {
      this.messageService.message = "Fetching people, this might takes a minute ...";

      this.people = [];
      return new Observable<Person[]>((observer) => {
        // To access the current service instance 
        var service = this;
        this.fetchPeople('https://swapi.co/api/people/', function () {
          var filteredPeople = service.filterPeople(filter, service.people);
          observer.next(filteredPeople);
        }, function (err) {
          if (err) this.people = null;
          observer.complete();
          service.messageService.message = "People was fetched completely.";
        });
      });
    }
  }

  /**
   * return the given person from the already loaded people
   * @param id
   */
  public getPerson(id: number): Person {

    if (!this.people) return null; // when still people has not been fetched

    var thePerson = this.people.find((person) => person.id == id);

    // update the peopleFromSamePlanet property, because maybe until this time new persons have been fetched
    thePerson.peopleFromSamePlanet = this.people.filter(person =>
      person.planet &&
      thePerson.planet &&
      person.planet.name === thePerson.planet.name
      && person.id != thePerson.id);

    return thePerson;
  }
}
