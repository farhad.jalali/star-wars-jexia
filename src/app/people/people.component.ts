import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { PeopleService } from '../people.service';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  people: Person[];
  filterPhrase: string = '';

  constructor(
    private route: ActivatedRoute,
    private peopleService: PeopleService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getPeople();
  }

  sort(field: string) {
    this.people = this.peopleService.sortPeople(field);
  }

  filter(value) {
    this.filterPhrase = value;
    this.getPeople();
  }

  getPeople(): void {
    this.peopleService
      .getPeople(this.filterPhrase)
      .subscribe(people => this.people = people);
  }
}
