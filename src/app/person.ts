import { Planet } from './planet';

export class Person {
  id: number;
  name: string;
  gender: string;
  birth_year: string;

  // the numeric part of birth_year used for sorting and filtering 
  numericBirthYear: number;

  // the planet url, is used for fetching the planet object
  homeworld: string;
  planet: Planet;
  peopleFromSamePlanet: Person[];
  url: string;
}
