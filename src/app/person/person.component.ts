import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../person';
import { PeopleService } from '../people.service';
import { PlanetComponent } from '../planet/planet.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  @Input() person: Person;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private peopleService: PeopleService
  ) { }

  ngOnInit() {
    this.getPerson();
  }

  onclick(id) {
    // Angular has a bug on the router navigation, this is just an uggly workaround ;)
    this.router.navigateByUrl('/');
    setTimeout(() => {
      this.router.navigateByUrl('/person/' + id);
    }, 100);
  }

  getPerson(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.person = this.peopleService.getPerson(id);
  }
}
