export class Planet {
  name: string
  climate: string
  gravity: string
  population: string
  terrain: string
}
